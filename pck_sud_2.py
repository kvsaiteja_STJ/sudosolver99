
def tier_success_coord_gen(tier_on_r,tier_on_rc,val_seek,b_index,tier_inner_r,pos_rc):
    RT_c=[tier_on_r-1,val_seek-1]
    RcT_c=[tier_on_rc-1,val_seek-1]
    b_r_c=[b_index,val_seek-1]
    R_r_c=[tier_inner_r+(tier_on_r-1)*3,val_seek-1]
    Rc_r_c=[(tier_on_rc-1)*3+pos_rc[0]%3,val_seek-1]
    R_c=[RT_c[0]*3+tier_inner_r,RcT_c[0]*3+pos_rc[0]%3]
    Rc_c=[R_c[1],R_c[0]]
    B_c=[b_index,(R_c[0]%3)*3+R_c[1]%3]
    return([RT_c,RcT_c],[R_r_c,Rc_r_c,b_r_c],[R_c,Rc_c,B_c],val_seek)
    


#inputs order (Target tier, Target_r,Targetcompliment_r,B_r,key which is either H or V)
#if Target is H then Targetcompliment is V and vice versa
def tier_s_w_1(RT,R_r,Rc_r,B_r,B,key,neg_cache_list):
#(RT,R_r,Rc_r,B_r,key)=(HT,H_r,V_r,B_r,'H')
#(RT,R_r,Rc_r,B_r,key)=(VT,V_r,H_r,B_r,'V')
    tier_on_r=-1
    for i in range(3):
        try:
            val_seek=RT[i].index(2)+1
            tier_on_r=i+1
            if([key,val_seek,tier_on_r] not in neg_cache_list):
                break
        except:
            tier_on_r=-1

    if(tier_on_r!=-1):
        #respective complimentary tier getter
        if(key=='H'):
            for i in range((tier_on_r-1)*3,tier_on_r*3):
                if(B_r[i][val_seek-1]==0):
                    tier_on_rc=i%3+1
                    break
        elif(key=='V'):
            for i in range(tier_on_r-1,9,3):
                if(B_r[i][val_seek-1]==0):
                    tier_on_rc=i//3+1
                    break

        ######--part 2-- 3 elements finder######
        for i in range((tier_on_r-1)*3,tier_on_r*3):
            if(R_r[i][val_seek-1]==0):
                tier_inner_r=i
                break

        if(key=='H'):
            b_index=(tier_on_r-1)*3+tier_on_rc-1
            tier_inner_r=tier_inner_r%3
            ele3=B[b_index][tier_inner_r*3:(tier_inner_r+1)*3]
        elif(key=='V'):
            b_index=(tier_on_rc-1)*3+tier_on_r-1
            tier_inner_r=tier_inner_r%3
            ele3=B[b_index][tier_inner_r::3]




        ######--part 3 Success or Hummm -- ######

        if(ele3.count(0)>1):
            pos_rc=[]
            for i in range(len(ele3)):
                if(ele3[i]==0):
                    pos_rc.append((tier_on_rc-1)*3+i)
            pos_rc_dum=pos_rc[:]
            for i in pos_rc_dum:
                if(Rc_r[i][val_seek-1]==1):
                    pos_rc.remove(i)
            if(len(pos_rc)==1):
                #print("success")
                #print(tier_on_r,tier_on_rc,val_seek,b_index,ele3)
                opl=[tier_on_r,tier_on_rc,val_seek,b_index,tier_inner_r,pos_rc]
                #print(opl)
                opv=tier_success_coord_gen(tier_on_r,tier_on_rc,val_seek,b_index,tier_inner_r,pos_rc)
                return (opv,neg_cache_list,[tier_on_r,tier_on_rc,val_seek,b_index,ele3])
            else:
                #print("Humm")
                #print(tier_on_r,tier_on_rc,val_seek,b_index,ele3)
                #pass
                neg_cache_list.append([key,val_seek,tier_on_r])
                return(None,neg_cache_list,[tier_on_r,tier_on_rc,val_seek,b_index,ele3])

        elif(ele3.count(0)==1):
            ind_0=ele3.index(0)
            pos_rc=[(tier_on_rc-1)*3+ind_0]
            opv=tier_success_coord_gen(tier_on_r,tier_on_rc,val_seek,b_index,tier_inner_r,pos_rc)
            return (opv,neg_cache_list,[tier_on_r,tier_on_rc,val_seek,b_index,ele3])
        
        else:
            return (None,neg_cache_list,[tier_on_r,tier_on_rc,val_seek,b_index,ele3])

    else:
        return (None,neg_cache_list,None)


