import random

def vect_success_coord_gen(h_no,v_no,b_no,val_seek):
    #print(val_seek)
    H_r_c=[h_no,val_seek-1]
    V_r_c=[v_no,val_seek-1]
    B_r_c=[b_no,val_seek-1]
    H_c=[h_no,v_no]
    V_c=[v_no,h_no]
    B_c=[b_no,(h_no%3)*3+v_no%3]
    HT_c=[h_no//3,val_seek-1]
    VT_c=[v_no//3,val_seek-1]
    return([HT_c,VT_c],[H_r_c,V_r_c,B_r_c],[H_c,V_c,B_c],val_seek)




#//picks a vector and returns vector and index and key(H,V,B)
def vector_picker(H_V_c,V_V_c,B_V_c,H,V,B,H_r,V_r,B_r,dot_num_staticsize_cache,num_dot_cache):
    #x=random.randint(0,1)
    x=0
    if(x==0):
        y=random.randint(0,1)
        if(y==0):
            key='H'
        elif(y==1):
            key='V'
        elif(y==2):
            key='B'
        ind=random.randint(0,8)
        #(key,ind)=('H',1)
        opv_v_p=vectorwise_w_1(ind,key,dot_num_staticsize_cache,num_dot_cache,H,V,B,H_r,V_r,B_r)
        #print(opv_v_p[-1])
    '''
    elif(x==1):
        y=random.radint(0,2)
        if(y==0):
            max_i=H_V_c.index(max(H_V_c))
            opv_v_p=vectorwise_w_1(max_i,'H',dot_num_staticsize_cache,num_dot_cache,H,V,B,H_r,V_r,B_r)
        elif(y==1):
            max_i=V_V_c.index(max(V_V_c))
            opv_v_p=vectorwise_w_1(max_i,'V',dot_num_staticsize_cache,num_dot_cache,H,V,B,H_r,V_r,B_r)
        elif(y==2):
            max_i=B_V_c.index(max(B_V_c))
            opv_v_p=vectorwise_w_1(max_i,'B',dot_num_staticsize_cache,num_dot_cache,H,V,B,H_r,V_r,B_r)
    ''' 
    suc_op_flg=-1
    if(opv_v_p!= -1):
        dot_num_oplist=[]
        [dot_num_staticsize_cache,num_dot_cache,dot_list]=opv_v_p
        if(dot_list!=[]):
            for [h_no,v_no,b_no] in dot_list: 
                suc_op_flg=1
                val_seek=dot_num_staticsize_cache[h_no][v_no].index(1)+1
                suc_op=vect_success_coord_gen(h_no,v_no,b_no,val_seek)
                #print('success')
                dot_num_oplist.append(suc_op)
                #dot_num_staticsize_cache,num_dot_cache)
            
        num_dot_oplist=[]
        for i in (num_dot_cache[-1][-1]):
            if(len(i[-1])==1):
                #//success
                suc_op_flg=1
                val_seek=i[0]
                #print(i[-1])
                [h_no,v_no,b_no]=i[-1][0]
                num_dot_oplist.append(vect_success_coord_gen(h_no,v_no,b_no,val_seek))

    if(suc_op_flg==1):
        return(dot_num_oplist,num_dot_oplist,dot_num_staticsize_cache,num_dot_cache)
    else:
        return(None,None,dot_num_staticsize_cache,num_dot_cache)
        
    
###################################################################
def vectorwise_w_1(ind,key,dot_num_staticsize_cache,num_dot_cache,H,V,B,H_r,V_r,B_r):
    the_big_break=-1
    dot_list=[]
    #print(key,ind)
    if(key=='H'):
        targetvalues=[]
        for i in range(9):
            if(H_r[ind][i]==0):
                targetvalues.append(i+1)
        if(targetvalues==[]):
            return(-1)
                
        vector=H[ind]
        targetloc=[]
        for i in range(9):
            if(vector[i]==0):
                targetloc.append(i)
        
        #print(targetloc)
        
        for i in targetloc:
            b_no=(ind//3)*3+i//3
            for j in targetvalues:
                if([B_r[b_no][j-1],V_r[i][j-1]]==[0,0]):
                    #print(dot_num_staticsize_cache[ind][i][j-1])
                    if(dot_num_staticsize_cache[ind][i][j-1]==0):
                        #print(ind,i,j-1)
                        dot_num_staticsize_cache[ind][i][j-1]=1
            if(dot_num_staticsize_cache[ind][i].count(1)==1):
                dot_list.append([ind,i,b_no])
        #//<num_dot_cache_append>//#
        s_l=[]
        for i in targetvalues:
            l_i=[]
            for j in targetloc:
                h_no=ind
                v_no=j
                b_no=(h_no//3)*3+(v_no//3)
                if([H_r[h_no][i-1],V_r[v_no][i-1],B_r[b_no][i-1]]==[0,0,0]):
                    l_i.append([h_no,v_no,b_no])

            s_l.append([i,l_i])
        num_dot_cache.append([key,ind,s_l])
        
    #'''          
    if(key=='V'):
        targetvalues=[]
        for i in range(9):
            if(V_r[ind][i]==0):
                targetvalues.append(i+1)
        if(targetvalues==[]):
            return(-1)
                
        vector=V[ind]
        targetloc=[]
        for i in range(9):
            if(vector[i]==0):
                targetloc.append(i)
        
        for i in targetloc:
            b_no=(i//3)*3+ind//3
            for j in targetvalues:
                if([B_r[b_no][j-1],H_r[i][j-1]]==[0,0]):
                    if(dot_num_staticsize_cache[i][ind][j-1]==0):
                        dot_num_staticsize_cache[i][ind][j-1]=1
            if(dot_num_staticsize_cache[ind][i].count(1)==1):
                dot_list.append([i,ind,b_no])
                        
        s_l=[]
        for i in targetvalues:
            l_i=[]
            for j in targetloc:
                v_no=ind
                h_no=j
                b_no=(h_no//3)*3+(v_no//3)
                if([H_r[h_no][i-1],V_r[v_no][i-1],B_r[b_no][i-1]]==[0,0,0]):
                    l_i.append([h_no,v_no,b_no])

            s_l.append([i,l_i])
        num_dot_cache.append([key,ind,s_l])

    if(key=='B'):
        targetvalues=[]
        for i in range(9):
            if(B_r[ind][i]==0):
                targetvalues.append(i+1)
        if(targetvalues==[]):
            return(-1)
                
        vector=B[ind]
        targetloc=[]
        for i in range(9):
            if(vector[i]==0):
                targetloc.append(i)
        
        for i in targetloc:
            h_no=ind//3+i//3
            v_no=(ind%3)*3+(i)%3
            for j in targetvalues:
                if([H_r[h_no][j-1],V_r[v_no][j-1]]==[0,0]):
                    if(dot_num_staticsize_cache[h_no][v_no][j-1]==0):
                        dot_num_staticsize_cache[h_no][v_no][j-1]=1
            if(dot_num_staticsize_cache[ind][i].count(1)==1):
                dot_list.append([h_no,v_no,ind])
                        

        s_l=[]
        for i in targetvalues:
            l_i=[]
            for j in targetloc:
                b_no=ind
                h_no=(b_no//3)*3+i//3
                v_no=(b_no%3)*3+i%3
                
                if([ [h_no][i-1],V_r[v_no][i-1],B_r[b_no][i-1]]==[0,0,0]):
                    l_i.append([h_no,v_no,b_no])

            s_l.append([i,l_i])
        num_dot_cache.append([key,ind,s_l])
        
    #'''
    #print([h_no])
    #return(dot_num_staticsize_cache,num_dot_cache,[h_no,v_no,b_no])
    return(dot_num_staticsize_cache,num_dot_cache,dot_list)

        
        
        