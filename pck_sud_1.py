def ch_vectors_gen(s):
    H=[]
    for i in s:
        h_i=[]
        for j in i:
            try:
                h_i.append(int(j))
            except:
                h_i.append(0)
        H.append(h_i)
    V=[]
    for i in range(9):
        v_i=[]
        for j in range(9):
            v_i.append(H[j][i])
        V.append(v_i)
    B=[[],[],[],[],[],[],[],[],[]]
    for i in range(9):
        for j in range(9):
            b_a,b_b=(i//3,j//3)
            B[3*b_a+b_b].append(H[i][j])

    return(H,V,B)

def vector_wise_reports(H,V,B):
    H_r=[]
    V_r=[]
    B_r=[]
    for i in H:
        H_r_in=[0,0,0,0,0,0,0,0,0]
        for j in i:
            if(j!=0):
                H_r_in[j-1]+=1
        H_r.append(H_r_in)

    for i in V:
        V_r_in=[0,0,0,0,0,0,0,0,0]
        for j in i:
            if(j!=0):
                V_r_in[j-1]+=1
        V_r.append(V_r_in)

    for i in B:
        B_r_in=[0,0,0,0,0,0,0,0,0]
        for j in i:
            if(j!=0):
                B_r_in[j-1]+=1
        B_r.append(B_r_in)

    return(H_r,V_r,B_r)

def V_count_r(H,V,B):
    H_V_c=[]
    V_V_c=[]
    B_V_c=[]
    for i in H:
        H_V_c.append(sum(i))
    for i in V:
        V_V_c.append(sum(i))
    for i in B:
        B_V_c.append(sum(i))
    return(H_V_c,V_V_c,B_V_c)
     
    
def bhv_getter(x,y):
    b_a,b_b=(x//3,y//3)
    b=3*b_a+b_b
    return(x,y,b)

def Tier_wise_counts(V,H):
    VT=[[0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0]]
    for i in range(9):
        for j in range(9):
            x=i//3
            if(V[i][j]!=0):
                VT[x][V[i][j]-1]+=1


    HT=[[0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0,0]]
    for i in range(9):
        for j in range(9):
            x=i//3
            if(H[i][j]!=0):
                HT[x][H[i][j]-1]+=1

    return(VT,HT)
